import Head from "next/head";
import { AppProps } from "next/app";
import { FunctionComponent } from "react";

const App: FunctionComponent<AppProps> = ({ Component, pageProps }) => (
  <>
    <Head>
      <title>Contador de votos</title>
      <link rel="icon" type="image/jpg" href="/favicon.ico" />
    </Head>
    <Component {...pageProps} />
  </>
);
export default App;
