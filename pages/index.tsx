import Image from "next/image";
import BallotIcon from "public/ballot-icon.png";
import RemoveIcon from "public/remove-icon.jpg";
import ResetIcon from "public/reset-icon.png";
import { FunctionComponent, useEffect, useState } from "react";

const ICON_SIZE = 30;

const IndexPage: FunctionComponent = () => {
  const [votesCount, setCount] = useState(0);

  useEffect(() => {
    setCount(Number(localStorage.getItem("contadorVotos") ?? 0));
  }, []);

  useEffect(() => {
    localStorage.setItem("contadorVotos", votesCount + "");
  }, [votesCount]);

  return (
    <main>
      <h1>Contador de votos 🗳</h1>
      <div>Cantidad de votos: {votesCount}</div>
      <button
        onClick={() => {
          setCount(votesCount + 1);
        }}
        style={{ width: "100%", height: 40 }}
      >
        <Image src={BallotIcon} width={ICON_SIZE} height={ICON_SIZE} />
      </button>
      <button
        onClick={() => {
          setCount(votesCount > 0 ? votesCount - 1 : 0);
        }}
        style={{ width: "100%", height: 40, marginTop: "50px" }}
      >
        <Image src={RemoveIcon} width={ICON_SIZE} height={ICON_SIZE} />
      </button>
      <button
        onClick={() => {
          setCount(0);
        }}
        style={{ width: "100%", height: 40, marginTop: "50px" }}
      >
        <Image src={ResetIcon} width={ICON_SIZE} height={ICON_SIZE} />
      </button>
    </main>
  );
};

export default IndexPage;
